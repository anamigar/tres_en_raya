import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    visible: true
    width: Screen
    height: Screen
    title: qsTr("Cuadrados")
    color: "white"
    property int init: 0

    Column{
        Row{
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked:{ text1.cambio(); enabled=false}


                }
                Text{
                    id:text1
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){

                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}




                    }
                }
            }
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked: {text2.cambio();enabled=false}
                }
                Text{
                    id:text2
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){
                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}


                    }
                }

            }
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked: {text3.cambio();enabled=false}
                }
                Text{
                    id:text3
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){
                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}


                    }
                }
            }



        }
        Row{
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked: {text4.cambio();enabled=false}
                }
                Text{
                    id:text4
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){
                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}


                    }
                }

            }
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked: {text5.cambio();enabled=false}
                }

                Text{
                    id:text5
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){
                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}


                    }
                }

            }
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked:{ text6.cambio();enabled=false}
                }
                Text{
                    id:text6
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){
                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}


                    }
                }

            }

        }Row{
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked: {text7.cambio();enabled=false}
                }
                Text{
                    id:text7
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){
                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}


                    }
                }

            }
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked: {text8.cambio();enabled=false}
                }
                Text{
                    id:text8
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){
                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}


                    }
                }

            }
            Rectangle{
                width:(Window.width)/3
                height:(Window.height)/3
                color: "white"
                border.color: "orange"
                border.width: 1
                MouseArea{
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {parent.border.width=16}
                    onExited: {parent.border.width=1}
                    onClicked: {text9.cambio();enabled=false}
                }
                Text{
                    id:text9
                    text:""
                    anchors.centerIn: parent
                    font.pixelSize: (Window.width/100)*8
                    function cambio(){
                        if(init!= 0){ text="<b>o<\b>"; init=0;parent.color="orange"}else{text="<b>x</b>";init=1;parent.color="yellow";parent.border.color="yellow"}



                    }
                }

            }

        }

    }

}

